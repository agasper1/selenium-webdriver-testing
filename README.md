# Configuring local environment


## 1. Install selenium 
> Install selenium (assumes you already have pip)
> `pip3 install selenium` 

> (I installed python3 with `homebrew` because python2.7 was already installed, and instead of messing with the versions, I aliased `python3` to be a thing) 
> (pip3 => my alias for pip)  

In your terminal: 
```
> python3
> help("modules")
```
(There's probably a better way to filter but anyway)

Look for the `selenium` module. 
(Also `ctrl+D` should get you out of the interactive python shell)

## 2. Install the browser web-drivers


You may likely get this error message out of the box: 
```
selenium.common.exceptions.WebDriverException: Message: 'geckodriver' executable needs to be in PATH. 
```

> Fair enough

> [Firefox Drivers](https://github.com/mozilla/geckodriver/releases)  
> Pick this one: `geckodriver-v0.21.0-macos.tar.gz`  
> In the `/var/www/html/selenium-sandbox` directory  
> `sudo tar -zxvf ~/Downloads/geckodriver-v0.21.0-macos.tar.gz`  
> `sudo chown andresgasper:admin ./geckodriver`  
> Create a folder for the browser-drivers:  
> `mkdir /var/www/html/browser-drivers`  
> Move the geckodriver to the `browser-drivers/` directory:  
> `mv geckodriver* /var/www/html/browser-drivers/`   
> Open wherever you set your path variables (personally: ~/.profile)
> Add a line for where you're storing the browser-drivers:  
```
# Add browser drivers to path
export PATH=$PATH:/var/www/html/browser-drivers
``` 
> `source ~/.profile`


> [Chrome Drivers](https://chromedriver.storage.googleapis.com/index.html?path=2.41/)  
> Pick: `chromedriver_mac64.zip`   
> Unzip with tool of your choice (I just used the generic mac utility)  
> Move the `chromedriver` to `browser-drivers/`  
> `mv ~/Downloads/chromedriver /var/www/html/browser-drivers` 


## Testing the installation 
> Verify that the Chrome browser works  
> `python3 ./chrome-selenium-test.py`

> Verify that the Firefox browser works  
> `python3 ./firefox-selenium-test.py` 
