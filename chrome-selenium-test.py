from selenium import webdriver
from selenium.webdriver.common import keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0

from save_log import saveLog

# setup functionality 
def setUp(): 
    chrome_options = Options()
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("start-maximized")
    # Create a new instance of the Chrome driver
    driver = webdriver.Chrome(
        chrome_options=chrome_options,
        desired_capabilities={'loggingPrefs': setUpPerfLoggingPrefs()})

    return driver
    
# Setup performance logging preferences
def setUpPerfLoggingPrefs():
    loggingPrefs = {'browser': 'ALL', 'driver': 'ALL', 'performance': 'ALL'}
    return loggingPrefs

driver = setUp()

# go to the google home page
# go to the page 
SITE_URL = 'http://localhost:3000'
driver.get(SITE_URL)
# driver.get("http://www.google.com")

try:
    
    performance_log = driver.get_log('performance')
    browser_log = driver.get_log('browser')
    driver_log = driver.get_log('driver')
    
    saveLog('performance', performance_log)
    saveLog('browser', browser_log)
    saveLog('driver', driver_log)
    print(driver.title)

finally:
    driver.quit()