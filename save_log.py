import time
# Makes a timestamp with the current time in YYYYMMDD_HHMMSS format 
def makeTimeStamp():
    time_string = time.strftime("%Y%m%d-%H%M%S")
    return time_string

# Makes a file in the format of typeOfLog_timestamp.txt 
def makeFileName(typeOfLog):
    fileName = typeOfLog + "_" + makeTimeStamp() + ".txt"
    return fileName

def saveLog(typeOfLog, log): 
    file = open(makeFileName(typeOfLog), "w+")
    for item in log:
        file.write('%s\n' % item)
    